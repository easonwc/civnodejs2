const express = require('express');
const NodeCache = require('node-cache');
const uuid4 = require('uuid');
const httpStatus = require('http-status-codes');

const router = express.Router();
const myCache = new NodeCache();
const ttlSeconds = 10000;

/* GET players listing. */
router.get('/', (req, res) => {
  const playerID = req.query.id;
  if (playerID === undefined) {
    res.status(httpStatus.BAD_REQUEST).send('bad request');
  } else {
    const player = myCache.get(playerID);
    if (player === undefined) {
      res.status(httpStatus.NOT_FOUND).send('not found');
    } else {
      res.send(player);
    }
  }
});

/* POST players listing. */
router.post('/initialize', (req, res) => {
  const newPlayerID = uuid4.v4();
  const fName = req.query.firstName !== undefined ? req.query.firstName : '';
  const lName = req.query.lastName !== undefined ? req.query.lastName : '';
  const player = { id: newPlayerID, firstName: fName, lastName: lName };
  myCache.set(newPlayerID, player, ttlSeconds);
  res.send(player);
});

/* PUT players listing. */
router.put('/', (req, res) => {
  const playerData = req.body;
  if (myCache.has(playerData.id)) {
    myCache.set(playerData.id, playerData, ttlSeconds);
    res.send(playerData);
  } else {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send('operation unsuccessful');
  }
});

/* DELETE players listing. */
router.delete('/', (req, res) => {
  const playerID = req.query.id;
  if (playerID === undefined) {
    res.status(httpStatus.BAD_REQUEST).send('bad request');
  } else {
    myCache.del(playerID);
    res.status(httpStatus.OK).send('operation successful');
  }
});

module.exports = router;

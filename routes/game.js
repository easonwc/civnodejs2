const express = require('express');
const NodeCache = require('node-cache');
const uuid4 = require('uuid');
const httpStatus = require('http-status-codes');

const router = express.Router();
const myCache = new NodeCache();
const ttlSeconds = 10000;

/* GET games listing. */
router.get('/', (req, res) => {
  const gameID = req.query.id;
  if (gameID === undefined) {
    res.status(httpStatus.BAD_REQUEST).send('bad request');
  } else {
    const game = myCache.get(gameID);
    if (game === undefined) {
      res.status(httpStatus.NOT_FOUND).send('not found');
    } else {
      res.send(game);
    }
  }
});

/* POST games listing. */
router.post('/initialize', (req, res) => {
  const newGameID = uuid4.v4();
  const game = { id: newGameID, players: [] };
  myCache.set(newGameID, game, ttlSeconds);
  res.send(game);
});

/* PUT games listing. */
router.put('/', (req, res) => {
  const data = req.body;
  if (myCache.has(data.id)) {
    myCache.set(data.id, data, ttlSeconds);
    res.send(data);
  } else {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send('operation unsuccessful');
  }
});

/* DELETE games listing. */
router.delete('/', (req, res) => {
  const gameID = req.query.id;
  if (gameID === undefined) {
    res.status(httpStatus.BAD_REQUEST).send('bad request');
  } else {
    myCache.del(gameID);
    res.status(httpStatus.OK).send('operation successful');
  }
});

module.exports = router;

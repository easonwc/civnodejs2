const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe('Game', () => {
  let gameID;

  describe('POST /', () => {
    // Test to create a single game record
    it('should create a new game', (done) => {
      chai.request(app)
        .post('/game/initialize')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          gameID = res.body.id;
          done();
        });
    });
    // Test to get single game record
    it('should get a single game record', (done) => {
      chai.request(app)
        .get(`/game?id=${gameID}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          done();
        });
    });
    // Test to not get single game record
    it('should get a single game record', (done) => {
      chai.request(app)
        .get('/game?id=00000000-0000-0000-0000-000000000000')
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
});
